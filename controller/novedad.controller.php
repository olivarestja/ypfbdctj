<?php 
require_once'model/novedad.php';

class NovedadController{

	private $model;
    
public function __CONSTRUCT()

	{
	
		$this->model=new Novedad();
    }

public function Index()

	{
	
	require_once 'view/header.php';
	require_once 'view/novedad/novedad.php';
	require_once 'view/footer.php';
	}

public function Crud(){
	$alm =new Novedad();
	if(isset($_REQUEST['codno']))
		{
			$alm=$this->model->Obtener($_REQUEST['codno']);
		}
	require_once 'view/header.php';
	require_once 'view/novedad/novedad-editar.php';
	require_once 'view/footer.php';
}

public function Guardar(){
$alm=new Novedad();

$alm->codno=$_REQUEST['codno'];
$alm->fecha=$_REQUEST['fecha'];
$alm->hora=$_REQUEST['hora'];
$alm->novedad=$_REQUEST['novedad'];

$alm->codno>0
? $this->model->Actualizar($alm)
: $this->model->Registrar($alm);
require_once 'view/header.php';
	require_once 'view/novedad/novedad.php';
	require_once 'view/footer.php';
}

public function Eliminar(){

$alm=new Novedad();
$this->model->Eliminar($_REQUEST['codno']);
require_once 'view/header.php';
	require_once 'view/novedad/novedad.php';
	require_once 'view/footer.php';

}

}


 ?>