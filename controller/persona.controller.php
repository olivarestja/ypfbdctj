<?php
require_once 'model/persona.php';

class PersonaController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new Persona();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/persona/persona.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Persona();
        
        if(isset($_REQUEST['codper'])){
            $alm = $this->model->Obtener($_REQUEST['codper']);
        }
        
        require_once 'view/header.php';
        require_once 'view/persona/persona-editar.php';
        require_once 'view/footer.php';
    }
    
    public function Guardar(){
        $alm = new Persona();
        
        $alm->codper = $_REQUEST['codper'];
        $alm->ci = $_REQUEST['ci'];
        $alm->nombre = $_REQUEST['nombre'];
        $alm->ap = $_REQUEST['ap'];
        $alm->am = $_REQUEST['am'];
        $alm->tipo = $_REQUEST['tipo'];
        
    $alm->codper>0
            ? $this->model->Actualizar($alm)
            : $this->model->Registrar($alm);
         require_once 'view/header.php';
         require_once 'view/persona/persona.php';
         require_once 'view/footer.php';
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['codper']);
        require_once 'view/header.php';
         require_once 'view/persona/persona.php';
         require_once 'view/footer.php';
    }
}