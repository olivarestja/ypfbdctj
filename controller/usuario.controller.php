<?php 
require_once 'model/usuario.php';

class UsuarioController{
private $model;

public function __Construct(){
	$this->model= new Usuario();
}

public function Index(){
	require_once 'view/header.php';
	require_once 'view/usuario/usuario.php';
	require_once 'view/footer.php';
}

public function Crud(){
	$usu=new Usuario();
        require_once 'view/header.php';
        require_once 'controller/persona.controller.php';
        require_once 'view/usuario/usuario-editar.php';
        require_once 'view/footer.php';
    }

public function Guardar(){
	$usu=new Usuario();
	$usu->codusu=$_REQUEST['codusu'];
	$usu->codper=$_REQUEST['codper'];
	$usu->login=$_REQUEST['login'];
	$usu->clave=$_REQUEST['clave'];
	$usu->codusu>0
            ? $this->model->Actualizar($usu)
            : $this->model->Registrar($usu);
        require_once 'view/header.php';
         require_once 'view/usuario/usuario.php';
         require_once 'view/footer.php';
}

}
 ?>
