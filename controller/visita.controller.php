<?php 
require_once 'model/visita.php';

class VisitaController{

	private $model;

public function __CONSTRUCT() {

	$this->model=new Visita();
}

public function Index(){

	require_once 'view/header.php';
	require_once 'view/visita/visita.php';
	require_once 'view/footer.php';
}

public function Crud(){

	$vta = new Visita();

	if (isset($_REQUEST['codvi'])) {
		$vta=$this->model->Obtener($_REQUEST['codvi']);
	}

	require_once 'view/header.php';
	require_once 'controller/persona.controller.php';
	require_once 'view/visita/visita-editar.php';
	require_once 'view/footer.php';
	
}

public function Guardar(){

	$vta=new Visita();

	$vta->codvi=$_REQUEST['codvi'];
	$vta->personas=implode(",",$_REQUEST['personas']);
	$vta->codper=$_REQUEST['codper'];
	$vta->motivo=$_REQUEST['motivo'];
	$vta->fecha=$_REQUEST['fecha'];
	$vta->hora=$_REQUEST['hora'];

	$vta->codvi>0
			?$this->model->Actualizar($vta)
			:$this->model->Registrar($vta);
			header('Location: index.php');
}

public function Eliminar(){
	$this->model->Eliminar($_REQUEST['codvi']);
	header('Location: index.php');
}
}

 ?>