<?php 
class Novedad {

private $pdo;

public  $codno;
public  $fecha;
public  $hora;
public  $novedad;



public function __CONSTRUCT()
	{

	try{
		$this->pdo=Database::StartUp();
		}
	catch(Exception $e){
		die($e->getMessage());
		}
	}

public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM novedades");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

public function Obtener($codno)
	{
		try{
				$stm=$this->pdo->prepare("SELECT * FROM novedades WHERE codno=?");
				$stm->execute(array($codno));
				return $stm->fetch(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

public function Eliminar($codno)
	{
		try{

			$stm=$this->pdo->prepare("DELETE FROM novedades WHERE codno=?");
			$stm->execute(array($codno));
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

public function Actualizar($data)
	{
		try{
			$sql=" UPDATE novedades SET
			
						fecha	=?,
						hora	=?,
						novedad	=?
				  WHERE codno	=?";
			$this->pdo->prepare($sql)
			->execute(
				array(

						$data->fecha,
						$data->hora,
						$data->novedad,
						$data->codno
						)
				);
		}
		catch(Exception $e){
			die($e->getMessage());
		}

	}

public function Registrar(Novedad $data)
	{
		try{

			$sql="INSERT INTO novedades (fecha,hora,novedad) 
					VALUES(?,?,?)";
					
			$this->pdo->prepare($sql)->execute(array(

					$data->fecha,
					$data->hora,
					$data->novedad
				));
					
				
		}

		catch (Exception $e){
			die($e->getMessage());
		}
	}		


}
 ?>

