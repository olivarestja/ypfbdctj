<?php
class Persona
{
	private $pdo;
    
	public $codper;
    public $ci;
    public $nombre;
    public $ap;
    public $am;
    
   

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM persona ORDER BY ap ASC");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

public function ListarYPFB()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM persona WHERE tipo='1' ORDER BY ap ASC");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

public function ListarOTROS()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM persona WHERE tipo='2' ORDER BY ap ASC");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function Obtener($codper)
	{
		try 
		{
			$stm = $this->pdo->prepare("SELECT * FROM persona WHERE codper = ?");
			$stm->execute(array($codper));
			return $stm->fetch(PDO::FETCH_OBJ);
		} 
		catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($codper)
	{
		try 
		{
			$stm = $this->pdo->prepare("DELETE FROM persona WHERE codper = ?");			          
			$stm->execute(array($codper));
		} 
		catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE persona SET 
						ci				=?,
						nombre          = ?, 
						ap      		= ?,
                        am        		= ?,
                        tipo            =?
						
				  WHERE codper 			= ?";

			$this->pdo->prepare($sql)->execute( array(
				    	
				    	$data->ci,
				    	$data->nombre, 
                        $data->ap,
                        $data->am,
                        $data->tipo,
                        $data->codper
                      
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Persona $data)
	{
		try 
		{
		$sql = "INSERT INTO persona (ci,Nombre,Ap,Am,tipo) 
		        VALUES (?, ?, ?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
					
					$data->ci,
                    $data->nombre,
                    $data->ap, 
                    $data->am,
                    $data->tipo
                   
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}