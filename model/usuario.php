<?php 
class Usuario{
	private $pdo;

	public $codusu;
	public $codper;
	public $login;
	public $clave;

	public function __Construct(){

		try{
			$this->pdo=Database::StartUp();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){

		try{
			$result=array();
			$stm=$this->pdo->prepare("SELECT * FROM usuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);

		}

		catch(Exception $e){
			die($e->getMessage());

		}

	}

	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE usuario SET 
						codper				=?,
						login          = ?, 
						clave      		= ?
                                
						
				  WHERE codusu 			= ?";

			$this->pdo->prepare($sql)->execute( array(
				    	
				    	$data->codusu,
				    	$data->codper,
				    	$data->login, 
                        $data->clave

                      
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Usuario $data){
		try{
			$sql="INSERT INTO usuario (codper,login,clave) VALUES (?,?,?)";
			$this->pdo->prepare($sql)->execute(array(
				$data->codper,
				$data->login,
				$data->clave
				));

		}
		catch (Exception $e){
			die($e->getMessage());
		}
	}
	
}
 ?>
