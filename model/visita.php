<?php 

class Visita{

	private $pdo;
	public  $codvi;
	public $codper;
	public $motivo;
	public $fecha;
	public $hora;

	public function __CONSTRUCT(){

		try{
				$this->pdo = Database::StartUp();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar($init  =FALSE, $page_size= FALSE){

		try{

			$result=array();

			$vis=$this->pdo->prepare(" SELECT visita.codvi, visita.fecha, visita.hora, visita.motivo, visita.personas, persona.nombre, persona.ap, persona.am FROM visita, persona WHERE visita.codper = persona.codper LIMIT :init, :size");
			$vis->bindParam(":init", $init, PDO::PARAM_INT);
			$vis->bindParam(":size", $page_size, PDO::PARAM_INT);
			$vis->execute();
			return $vis->fetchAll(PDO::FETCH_OBJ);
		}
	catch(Exception $e){
		die($e->getMessage());
	}
	}


	public function Obtener($codvi){

		try{
				$vis=$this->pdo->prepare("SELECT * FROM visita WHERE codvi=?");
				$vis->execute(array($codvi));
				return $vis->fetch(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Eliminar($codvi){

		try{

			$vis=$this->pdo->prepare("DELETE FROM visita WHERE codvi=?");
			$vis->execute(array($codvi));

		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Actualizar($datos){
		try{

			$sql="UPDATE visita SET 

						personas=?,
						motivo  =?, 
						fecha	=?, 
						hora	=?  
						WHERE codvi=?";

			$this->pdo->prepare($sql)->execute(array(
				
				
				
				$datos->personas,
				$datos->motivo,
				$datos->fecha,
				$datos->hora,
				$datos->codvi
				)
			)
			;
		}

		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Registrar(Visita $datos){

		try{
				$sql="INSERT INTO visita (personas,codper,motivo,fecha,hora) VALUES(?,?,?,?,?) ";
				$this->pdo->prepare($sql)->execute(array($datos->personas,$datos->codper,$datos->motivo,$datos->fecha,$datos->hora));
		}

		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function counting_visita(){
		$vis=$this->pdo->prepare(" SELECT * FROM visita");
	    $vis->execute();
		return $vis = $vis->rowCount();
	}
}
 ?>
