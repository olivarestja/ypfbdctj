<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Control de Visitas</title>
        
        <meta charset="utf-8" />
        
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="assets/js/jquery-ui/jquery-ui.min.css" />
        <!--<link rel="stylesheet" href="assets/css/style.css" />-->
        
        <link rel="stylesheet" href="assets/css/select-chose/chosen.css">
        <link rel="stylesheet" href="assets/css/select-chose/prism.css">
        
        <link rel="stylesheet" href="assets/css/menu.css">


        
        
	</head>
    <body>
        
    <div class="container" >
    
    <ul class="menu" >
      <li class="menu"><a href="?c=Visita">Visitas</a></li>
      <li class="menu"><a href="?c=Novedad">Novedades</a></li>
      <li class="menu"><a href="?c=Persona">Personas </a></li>
      <li class="menu"><a href="?c=Usuario">Usuarios </a></li>
    </ul>