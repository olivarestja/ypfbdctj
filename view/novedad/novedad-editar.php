<h1 class="page-header">
    <?php
     echo $alm->codno != null ? $alm->fecha : 'Registrar Novedad'; 
     ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Novedad">Fecha</a></li>
  <li class="active"><?php echo $alm->codno != null ? $alm->fecha : 'Nuevo Registro'; ?></li>
 
</ol>

<form id="frm-alumno" action="?c=Novedad&a=Guardar" method="post" enctype="multipart/form-data">
    
    <input type="hidden" name="codno"  value="<?php echo $alm->codno; ?>" />
    
     <div class="form-group">
        <label>Fecha</label>
        <input type="text" name="fecha" value="<?php echo $alm->fecha; ?>" class="form-control" placeholder="Ingrese fechaI" data-validacion-tipo="requerido" />
    </div>

    <div class="form-group">
        <label>Hora</label>
        <input type="text" name="hora" value="<?php echo $alm->hora; ?>" class="form-control" placeholder="Ingrese la hora" data-validacion-tipo="requerido" />
    </div>
    
    <div class="form-group">
        <label>Novedad</label>
        <input type="text" name="novedad" value="<?php echo $alm->novedad; ?>" class="form-control" placeholder="Ingrese novedad" data-validacion-tipo="requerido" />
    </div>
   
    
    
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>