<h3 class="page-header">Novedades</h3>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Novedad&a=Crud">Nueva Novedad</a>
</div>



<table class="table table-striped">
    <thead>
        <tr>
        
            
            <th style="width:50px;">Cod</th>
            <th style="width:100px;">Fecha</th>
            <th style="width:100px;">Hora</th>
            <th style="width:100px;">Novedad</th>
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            
            <td><?php echo $r->codno; ?></td>
            <td><?php echo $r->fecha; ?></td>
            <td><?php echo $r->hora; ?></td>
            <td><?php echo $r->novedad; ?></td>
            
            <td>
                <a href="?c=Novedad&a=Crud&codno=<?php echo $r->codno; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=Novedad&a=Eliminar&codno=<?php echo $r->codno; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
