<h1 class="page-header">
    <?php
     echo $alm->codper != null ? $alm->nombre : 'Nueva Persona'; 
     ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Persona">Persona</a></li>
  <li class="active"><?php echo $alm->codper != null ? $alm->nombre : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-alumno" action="?c=Persona&a=Guardar" method="post" enctype="multipart/form-data">
    
    <input type="hidden" name="codper"  value="<?php echo $alm->codper; ?>" />
    
     <div class="form-group" style="width:23%">
        <label>CI</label>
        <input type="text" name="ci" value="<?php echo $alm->ci; ?>" class="form-control" placeholder="Ingrese su CI" data-validacion-tipo="requerido|min:3" />
    </div>

    <div class="form-group" style="width:23%">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $alm->nombre; ?>" class="form-control" placeholder="Ingrese su nombre" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group" style="width:23%">
        <label>Ap</label>
        <input type="text" name="ap" value="<?php echo $alm->ap; ?>" class="form-control" placeholder="Ingrese su apellido paterno" data-validacion-tipo="requerido" />
    </div>
    
    <div class="form-group" style="width:23%">
        <label>Am</label>
        <input type="text" name="am" value="<?php echo $alm->am; ?>" class="form-control" placeholder="Ingrese Apellido Materno"  />
    </div>

    <div class="form-group" style="width:23%">
        <label>Tipo</label>
        <select name="tipo" class="form-control">
            <option value="2">VISITA</option>
            <option value="1">YPFB</option>
        </select>
    </div>
    
    
    
    <hr />
    
    <div class="text-right" style="width:23%">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>