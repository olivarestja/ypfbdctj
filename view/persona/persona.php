
 <h3 class="page-header">Personas</h3> 
<div class="well well-sm text-right">
  <a class="btn btn-primary" href="?c=Persona&a=Crud">Nueva Persona</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            
            <th style="width:50px;">CI</th>
            <th style="width:100px;">Nombre</th>
            <th style="width:100px;">Ap</th>
            <th style="width:100px;">Am</th>
            <th style="width:100px;">Tipo</th>
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            
            <td><?php echo $r->ci; ?></td>
            <td><?php echo $r->nombre; ?></td>
            <td><?php echo $r->ap; ?></td>
            <td><?php echo $r->am; ?></td>
            <td>
            <?php 
            if ($r->tipo==1) {
                echo 'YPFB';
            }
            else{
            echo 'VISITA'; 
            }
            ?>

            </td>
            
            <td>
                <a href="?c=Persona&a=Crud&codper=<?php echo $r->codper; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=Persona&a=Eliminar&codper=<?php echo $r->codper; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>


    </tbody>
</table> 
 