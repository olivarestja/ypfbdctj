<h1 class="page-header">
    <?php
     echo $usu->codusu != null ? $usu->nombre : 'Nueva Persona'; 
     ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Persona">Persona</a></li>
  <li class="active"><?php echo $usu->codusu != null ? $usu->login : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-alumno" action="?c=Usuario&a=Guardar" method="post" enctype="multipart/form-data">
    
    <input type="hidden" name="codusu"  value="<?php echo $usu->codusu; ?>" />
    
     

    <div class="form-group" style="width:23%">
        <label>Login</label>
        <input type="text" name="login" value="<?php echo $usu->login; ?>" class="form-control" placeholder="Ingrese su nombre" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group" style="width:23%">
        <label>Clave</label>
        <input type="text" name="clave" value="<?php echo $usu->clave; ?>" class="form-control" placeholder="Ingrese su apellido paterno" data-validacion-tipo="requerido" />
    </div>
    
    
     <div class="form-group">
     <label>Autorizado Por</label><br>     
    <select  name="codper" data-placeholder="Buscar Persona..." class="chosen-select" >
    <option value=""></option>
    <?php 
    $this->persona=new Persona();
    foreach($this->persona->ListarYPFB() as $r): 
    ?>
    <option value="<?php echo $r->codper; ?>"><?php echo $r->ci, ' ',$r->nombre, ' ', $r->ap, ' ', $r->am; ?> </option>
    <?php endforeach?>
    </select>
    </div>
    
    
    <hr />
    
    <div class="text-right" style="width:23%">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>