
 <h3 class="page-header">Usuarios</h3> 
<div class="well well-sm text-right">
  <a class="btn btn-primary" href="?c=Usuario&a=Crud">Nuevo Usuario</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            
            <th style="width:50px;">Cod</th>
            <th style="width:100px;">login</th>
            <th style="width:100px;">clave</th>
           
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            
            <td><?php echo $r->codusu; ?></td>
            <td><?php echo $r->login; ?></td>
            <td><?php echo $r->clave; ?></td>
                        
            <td>
                <a href="?c=Usuario&a=Crud&codusu=<?php echo $r->codusu; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=Usuario&a=Eliminar&codusu=<?php echo $r->codusu; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>


    </tbody>
</table> 