

<!--<h3 class="page-header">
    <?php
     echo $vta->codvi != null ? $vta->fecha : 'Nueva Visita'; 
     ?>
</h3>
-->


<ol class="breadcrumb">
  <li><a href="?c=Visita">Visita</a></li>
  <li class="active"><?php echo $vta->codvi != null ? $vta->fecha : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-alumno" action="?c=Visita&a=Guardar" method="post" enctype="multipart/form-data">
    
    <input type="hidden" name="codvi"  value="<?php echo $vta->codvi; ?>" />
    
     

    <div class="form-group" style="width:23%">
        <label>Fecha</label>
        <input type="date" name="fecha" value="<?php echo $vta->fecha; ?>" class="form-control" />
    </div>
    <div class="form-group" style="width:23%">   
        <label>Hora</label>
        <input type="time" name="hora" value="<?php echo $vta->hora; ?>"  class="form-control" placeholder="Ingrese Hora" data-validacion-tipo="requerido" />
    </div>
    
    <div class="form-group">
     <label>Personas</label><br>
     <select  name="personas[]" data-placeholder="Buscar Persona..." class="chosen-select" multiple tabindex="-1" style="width:100%">
    <option value=""></option>
    <?php 
    $this->persona=new Persona();
    foreach($this->persona->ListarOTROS() as $r): 
    ?>
    <option value="<?php echo $r->ci, ' ',$r->nombre, ' ', $r->ap, ' ', $r->am; ?>"><?php echo $r->ci, ' ',$r->nombre, ' ', $r->ap, ' ', $r->am; ?> </option>
    <?php endforeach?>
    </select>
    </div>
    
    
    <div class="form-group">
        <label>Motivo</label>
        <textarea name="motivo" value="<?php echo $vta->motivo; ?>" class="form-control" data-validacion-tipo="requerido|min:3"></textarea>
        
    </div>
    
    <div class="form-group">
     <label>Autorizado Por</label><br>     
    <select  name="codper" data-placeholder="Buscar Persona..." class="chosen-select" >
    <option value=""></option>
    <?php 
    $this->persona=new Persona();
    foreach($this->persona->ListarYPFB() as $r): 
    ?>
    <option value="<?php echo $r->codper; ?>"><?php echo $r->ci, ' ',$r->nombre, ' ', $r->ap, ' ', $r->am; ?> </option>
    <?php endforeach?>
    </select>
    </div>

    
    
    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>
<hr />
<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>