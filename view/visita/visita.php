
    <!-- Paginacion -->
    <?php 
    $Numero = new Visita();
    $NVisita=$Numero->counting_visita();
    //tamaño de la pagina
   $page_size = 2;
    if(isset($_GET['pages'])){
        $page=$_GET['pages'];
        $init=($page-1)* $page_size;
    }
    else{
        $init=0;
        $page=1;
    }
    //calcular total de las paginas
    $total_pages=ceil($NVisita/$page_size);
    $result=$Numero->Listar($init, $page_size);  
    ?>
    
<!-- Fin de paginacion -->

<div class="well well-sm text-right">
     <a class="btn btn-primary " href="?c=Visita&a=Crud">Registrar</a>
</div>


<table class="table table-striped">
    <thead>
        <tr>
            
            
            <th style="width:100px;">Fecha</th>
            <th style="width:100px;">Hora</th>
            <th style="width:100px;">Personas</th>
            <th style="width:60px;">Motivo</th>
            <th style="width:60px;">Autorizacion</th>
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>

        </tr>
    </thead>
    <tbody>
    
    <?php 

         if($result){
         foreach($result as $key => $r){ ?>
        <tr>
            
            
            <td><?php echo $r->fecha; ?></td>
            <td><?php echo $r->hora; ?></td>
            <td><?php echo $r->personas; ?></td>
            <td><?php echo $r->motivo; ?></td>
            <td><?php echo $r->nombre,' ', $r->ap,' ', $r->am; ?></td>
            
            <td>
                <a href="?c=Visita&a=Crud&codvi=<?php echo $r->codvi; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=Visita&a=Eliminar&codvi=<?php echo $r->codvi; ?>">Eliminar</a>
            </td>
        </tr>



<?php }}
else{
    ?>
    <tr>
        <td colspan="7">No hay Visitas Registradas</td>
    </tr>
<?php } ?>
    
    
    
    </tbody>
</table> 

                <?php 

                if($total_pages>1){

                 if ($page!=1) {
                    $pagi=$page-1;
                    echo"<a href=?pages=$pagi&c=Visita> < </a>";
                 }

                 for($i=1; $i<=$total_pages;$i++){
                    if ($page==$i) {
                       echo $page;
                    }
                    else{
                    echo "<a href=?pages=$i&c=Visita>  $i </a>"; 
                    }
                 }
                }
                ?>
         
