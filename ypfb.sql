-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 01-08-2017 a las 20:01:23
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ypfb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE IF NOT EXISTS `novedades` (
  `codno` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(10) NOT NULL,
  `hora` varchar(8) NOT NULL,
  `novedad` varchar(255) NOT NULL,
  PRIMARY KEY (`codno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`codno`, `fecha`, `hora`, `novedad`) VALUES
(6, '222', '222', '222');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `codper` int(11) NOT NULL AUTO_INCREMENT,
  `ci` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ap` varchar(255) NOT NULL,
  `am` varchar(255) DEFAULT NULL,
  `tipo` int(11) NOT NULL DEFAULT '2',
  PRIMARY KEY (`codper`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`codper`, `ci`, `nombre`, `ap`, `am`, `tipo`) VALUES
(1, '5028842', 'GONZALO ADHEMAR', 'OLIVARES', 'SANTOS', 1),
(5, '9123544', 'JOSE MARIA', 'URQUIZO', 'MAMANI', 2),
(6, '5801675', 'NELSON ', 'AVILES ', 'ALTAMIRANO', 2),
(7, '4137326', 'MERY', 'ESTRADA', '', 2),
(8, '4141749', 'LINO SALOMON', 'MEDINA', 'SULLCA', 2),
(9, '4157614', 'JOSE LUIS', 'HUANCA', 'CONDORI', 2),
(10, 'E-13516792 (COL)', 'IVAN ', 'FAJARDO', 'NARANJO', 2),
(11, 'E-13862750 (COL)', 'IVAN ', 'FAJARDO', 'SANCHEZ', 2),
(12, '4151328', 'IRAIDA VIOLETA', 'HERRERA ', 'NUÑEZ', 2),
(13, '5783167', 'MARCO ANTONIO ', 'VILLENA', 'JURADO', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visita`
--

CREATE TABLE IF NOT EXISTS `visita` (
  `codvi` int(11) NOT NULL AUTO_INCREMENT,
  `personas` varchar(500) DEFAULT NULL,
  `codper` int(11) NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY (`codvi`),
  KEY `codper` (`codper`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `visita`
--

INSERT INTO `visita` (`codvi`, `personas`, `codper`, `motivo`, `fecha`, `hora`) VALUES
(6, '5801675 NELSON  AVILES  ALTAMIRANO', 1, 'reunion con distrital', '2017-07-25', '12:44:00'),
(7, '9123544 JOSE MARIA URQUIZO MAMANI', 1, 'CONSULTA SEGURIDAD INDUSTRIAL', '2017-07-25', '13:02:00'),
(8, '4137326 MERY ESTRADA ', 1, 'CONSULTDA ADQUISICIONES', '2017-07-25', '15:30:00'),
(9, 'E-13516792 (COL) IVAN  FAJARDO NARANJO', 1, 'VISITA SEGURIDAD INDUSTRIAL ', '2017-07-25', '16:04:00'),
(14, 'E-13516792 (COL) IVAN  FAJARDO NARANJO,E-13862750 (COL) IVAN  FAJARDO SANCHEZ', 1, 'SEGURIDAD INDUSTRIAL', '2017-07-25', '23:55:00'),
(15, '4137326 MERY ESTRADA ', 1, 'VISITA DONDE SEA', '2017-07-27', '11:01:00'),
(17, '4137326 MERY ESTRADA ', 1, '5555', '2017-07-27', '05:55:00'),
(18, '4137326 MERY ESTRADA ', 1, 'AAA', '2017-07-20', '22:02:00'),
(19, '4137326 MERY ESTRADA ', 1, 'REUNION CON GERENTE ', '2017-08-17', '11:11:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
